############################################## nginx builder container ##############################################
FROM oraclelinux:8-slim

ENV NODEJS_VERSION=14

RUN export ARCH=`uname -m | sed 's/x86_64/amd64/' | sed 's/aarch64/arm64/'` && \
    echo -e "[nodejs]\nname=nodejs\nstream=${NODEJS_VERSION}\nprofiles=\nstate=enabled\n" > /etc/dnf/modules.d/nodejs.module && \
    curl --silent --location https://dl.yarnpkg.com/rpm/yarn.repo | tee /etc/yum.repos.d/yarn.repo && \
    rpm --import https://dl.yarnpkg.com/rpm/pubkey.gpg && \
    microdnf install nodejs tar gzip wget git npm zip yarn && \
    cd /tmp ; wget https://golang.org/dl/go1.18.2.linux-${ARCH}.tar.gz && \
    cd /tmp ; tar xfzv go1.18.2.linux-${ARCH}.tar.gz -C /usr/local/ && \
    rm /tmp/go1.18.2.linux-${ARCH}.tar.gz && \
    yarn add @grafana/toolkit && \
    useradd -u 1001 -d /home/manteio -b /home/manteio -m manteio

USER 1001
RUN  echo -e "\nexport PATH=$PATH:/usr/local/go/bin" > /home/manteio/.bashrc